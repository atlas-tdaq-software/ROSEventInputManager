//$Id$
/****************************************************************/
/*								*/
/*  file exec_eventmanager.c					*/
/*								*/
/*  exerciser for the event manager package			*/
/*								*/
/*  020725  J.O.Petersen EP					*/
/*								*/
/****************************************************************/

#include <iostream>
#include <vector>
#include <iomanip>
#include <sys/types.h>
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSEventInputManager/EventDescriptor.h"
#include "ROSEventInputManager/EventInputManager.h"

using namespace ROS;

#define MAXED 10
#define MAX_J 1000

enum 
{
  CREATEMANAGER = 0x01,
  GETPCIADDRESSOFFREEPAGE,
  NUMBEROFFREEPAGES,
  GETEVENTDESCRIPTOR,
  GETEVENTDESCRIPTOR1,
  CREATE,
  GETBYID,
  DELETEBYID,
  DELETEBYIDVECTOR,
  RESET,
  GETPAR,
  GETMEMORYPOOL,
  GETNOHASHLISTS,
  GETHASHBASE,
  GETEDBASE,
  PRINTINTERNALS,
  DELETEMANAGER,
  DUMPEVENTDESCRIPTOR,
  PRINTEVENTMANAGERARRAY,
  TIMING,
  HELP,
  QUIT = 100
};

#define MAX_EVENTMANAGERS 10

/************/
int main(void)
/************/ 
{
  char ich[10],option;
  err_type ret;
  int quit_flag = 0, noRols, noPages, pageSize, noHashLists, noPagesReserved, l_noPages, l_noDeleteEvents, RolNo, l1Id,  emNo, error_find_flag;
  int emPoolNo[MAX_EVENTMANAGERS] = {0};
  u_int physAdd;
  tstamp ts1;
  tstamp ts2;
  float delta;
  double nsecpercall;

  std::vector<u_int> l1Ids;
  l1Ids.reserve(1);		// fixed size vector

  MemoryPool *memPool;
  MemoryPage *memPage;

  EventInputManager * em[MAX_EVENTMANAGERS] = {0};

  evDesc_t *ed_ptr;
  evDesc_t *edBase;

  do 
  {
    std::cout << std::endl << std::endl;
    std::cout << " create event manager (new): " << std::dec <<  CREATEMANAGER << std::endl;
    std::cout << " getPCIAddressOfFreePage   : " << std::dec <<  GETPCIADDRESSOFFREEPAGE << std::endl;
    std::cout << " numberOfFreePages         : " << std::dec <<  NUMBEROFFREEPAGES << std::endl;
    std::cout << " getEventDescriptor (memP) : " << std::dec <<  GETEVENTDESCRIPTOR << std::endl;
    std::cout << " getEventDescriptor (PA)   : " << std::dec <<  GETEVENTDESCRIPTOR1 << std::endl;
    std::cout << " create event              : " << std::dec <<  CREATE << std::endl;
    std::cout << " getById                   : " << std::dec <<  GETBYID << std::endl;
    std::cout << " deleteById                : " << std::dec <<  DELETEBYID << std::endl;
    std::cout << " deleteById  (vector)      : " << std::dec <<  DELETEBYIDVECTOR << std::endl;
    std::cout << " reset                     : " << std::dec <<  RESET << std::endl;
    std::cout << " getMemoryPool             : " << std::dec <<  GETMEMORYPOOL << std::endl;
    std::cout << " getNumberOfHashLists      : " << std::dec <<  GETNOHASHLISTS << std::endl;
    std::cout << " getHashBase               : " << std::dec <<  GETHASHBASE << std::endl;
    std::cout << " getEventDescriptorBase    : " << std::dec <<  GETEDBASE << std::endl;
    std::cout << " printInternals            : " << std::dec <<  PRINTINTERNALS << std::endl;
    std::cout << " delete                    : " << std::dec <<  DELETEMANAGER << std::endl;
    std::cout << " DUMPEVENTDESCRIPTOR       : " << std::dec <<  DUMPEVENTDESCRIPTOR << std::endl;
    std::cout << " PRINTEVENTMANAGERARRAY    : " << std::dec <<  PRINTEVENTMANAGERARRAY << std::endl;
    std::cout << " TIMING                    : " << std::dec <<  TIMING << std::endl;
    std::cout << " HELP                      : " << std::dec <<  HELP << std::endl;
    std::cout << " QUIT                      : " << std::dec <<  QUIT << std::endl;

    std::cout << " ? : ";
    option = getdec(); /* get an integer */

    switch(option)   
    {
    case CREATEMANAGER:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " # pages = ";
      noPages = getdecd(10);
      
      std::cout << " page size = ";
      pageSize = getdecd(4096);
      
      std::cout << " # hash lists (buckets) = ";
      noHashLists = getdecd(4);
      
      std::cout << " # pages reserved = ";
      noPagesReserved = getdecd(4);

      try 
      {
	em[emNo] = new EventInputManager(noPages, pageSize, noHashLists, MemoryPool::ROSMEMORYPOOL_CMEM_BPA, noPagesReserved);
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
      }

      std::cout << " the associated memory pool is @ " << em[emNo]->getMemoryPool() << std::endl;
      break;

    case GETPCIADDRESSOFFREEPAGE:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      try 
      {
	physAdd = em[emNo]->getPCIAddressOfFreePage();
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
      }

      std::cout << " PCI address of free page = " << std::hex << physAdd << std::endl;
      break;

    case NUMBEROFFREEPAGES:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      try 
      {
	l_noPages = em[emNo]->numberOfFreePages();
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
      }

      std::cout << " # free pages = " << l_noPages << std::endl;
      break;

    case GETEVENTDESCRIPTOR:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);
      
      std::cout << " memory page (descriptor, from getPage)  = ";
      memPage = (MemoryPage*) gethex();
      
      ed_ptr = em[emNo]->getEventDescriptor(memPage);
      std::cout << " get ED @ " << ed_ptr << std::endl;
      std::cout << " with page # = " << ed_ptr->pagenum << "  and memory page @ " << ed_ptr->myPage << std::endl;
      break;

    case GETEVENTDESCRIPTOR1:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);
      
      std::cout << " physical address of memory page (from getPage)  = ";
      physAdd = (u_int)gethex();

      ed_ptr = em[emNo]->getEventDescriptor(physAdd);
      std::cout << " get ED @ " << ed_ptr << std::endl;
      std::cout << " with page # = " << ed_ptr->pagenum << "  and memory page @ " << ed_ptr->myPage << std::endl;
      break;

    case CREATE:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " ED to create (in std::hex) = ";
      ed_ptr = (evDesc_t*)gethex();

      try 
      {
	em[emNo]->createEvent(ed_ptr);
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
      }
      break;

    case GETBYID:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " l1Id = ";
      l1Id = getdec();

      error_find_flag = 0;	// is this the way to do it ?

      try 
      {
	ed_ptr = em[emNo]->getById(l1Id);
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
	error_find_flag = 1;
      }

      if (error_find_flag == 0)
	std::cout << " found ED @  " << ed_ptr << "  with l1Id = " << ed_ptr->L1id << std::endl;
      break;

    case DELETEBYID:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " l1Id = ";
      l1Id = getdec();

      error_find_flag = 0;	// is this the way to do it ?

      try 
      {
	em[emNo]->deleteById(l1Id);
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
	error_find_flag = 1;
      }

      if (error_find_flag == 0)
	std::cout << " found ED with L1id = " << l1Id << "  and deleted it\n";
      break;

    case DELETEBYIDVECTOR:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " # events to delete ";
      l_noDeleteEvents = getdecd(1);   

      l1Ids.clear();
      for (int i = 0; i < l_noDeleteEvents; i++) 
      {
	std::cout << " l1Id = ";
	l1Id = getdec();
	l1Ids.push_back(l1Id);
      }

      error_find_flag = 0;	// is this the way to do it ?

      try 
      {
	em[emNo]->deleteById(&l1Ids);
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
	error_find_flag = 1;
      }

      if (error_find_flag == 0) 
	for (int i = 0; i < l_noDeleteEvents; i++) 
  	  std::cout << " found ED with L1id = " << l1Ids[i] << "  and deleted it\n";
      break;

    case RESET:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      em[emNo]->reset();
      break;

    case GETMEMORYPOOL:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " memoryPool (pointer) = " << em[emNo]->getMemoryPool() << std::endl;
      break;

    case GETNOHASHLISTS:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " # hash lists = " << em[emNo]->getNumberOfHashLists() << std::endl;
      break;

    case GETHASHBASE:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " base address of hash table = " << em[emNo]->getHashBase() << std::endl;
      break;

    case GETEDBASE:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      std::cout << " base address of event descriptor table = " << em[emNo]->getEventDescriptorBase() << std::endl;
      break;

    case PRINTINTERNALS:
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      em[emNo]->printInternals();
      break;

    case DELETEMANAGER: 
      std::cout << " event manager # = (<" << MAX_EVENTMANAGERS << ") ";
      emNo = getdecd(0);

      try 
      {
	delete em[emNo];
      }
      catch (ROSException& e) 
      {
	std::cout << " package: " << e.getPackage() << std::endl;
	std::cout << " error ID: " << e.getErrorId() << std::endl;
	std::cout << " Description: " << e.getDescription() << std::endl;
      }

      em[emNo] = 0;
      emPoolNo[emNo] = 0;
      break;

    case DUMPEVENTDESCRIPTOR:
      std::cout << " ED to dump (in std::hex) = ";
      ed_ptr = (evDesc_t*)gethex();

      std::cout << " L1id          = " << ed_ptr->L1id << std::endl;
      std::cout << " page #        = " << ed_ptr->pagenum << std::endl;
      std::cout << " next          = " << ed_ptr->next << std::endl;
      std::cout << " lid           = " << ed_ptr->lid << std::endl;
      std::cout << " memory page   = " << ed_ptr->myPage << std::endl;
      std::cout << " ROD frg size  = " << ed_ptr->RODFragmentSize << std::endl;
      std::cout << " ROD frg stat  = " << ed_ptr->RODFragmentStatus << std::endl;
      std::cout << "   VA          = " << ed_ptr->myPage->address() << std::endl;
      std::cout << "   PA          = " << std::hex << ed_ptr->myPage->physicalAddress() << std::dec << std::endl;
      std::cout << "   page #      = " << ed_ptr->myPage->pageNumber() << std::endl;
      std::cout << "   size        = " << ed_ptr->myPage->usedSize() << std::endl;
      break;

    case PRINTEVENTMANAGERARRAY:
      std::cout << " event manager  (pointer) in pool #  : " << std::endl;
      for (int i = 0; i < MAX_EVENTMANAGERS; i++) 
      {
	std::cout << std::setw(14) << i << "  " << std::hex << em[i];
	if (em[i])
          std::cout << std::setw(10) << emPoolNo[i];
	std::cout << std::endl;
      }
      break;

    case TIMING:
      // create and delete all Events in event manager # 0 :
      // this MUST be created by hand before measuring the timing

      TS_OPEN(10000, TS_DUMMY);

      emNo = 0;			// use EM # 0
      edBase = em[emNo]->getEventDescriptorBase();
      std::cout << " ED base address of EM # 0 : " << edBase << std::endl;

      memPool = em[emNo]->getMemoryPool();
      l_noPages = memPool->numberOfPages();
      std::cout << " # pages in pool # 0 : " << l_noPages << std::endl;

      ts_clock(&ts1);

      for (int j = 0; j < MAX_J; j++)           // just to make time longer .. 
      {
	ed_ptr = (evDesc_t*)edBase;		// first ED
	
	for (int i = 0; i < l_noPages; i++) 
	{
          em[emNo]->createEvent(ed_ptr);
          ed_ptr++;
	}

	for (int i = 0; i < l_noPages; i++) 
	{
          l1Id = i;  // !!!!
          em[emNo]->deleteById(l1Id);
	}
      }

      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2);

      nsecpercall = (delta / (l_noPages * MAX_J)) * 1000000000;
      std::cout << " nanosecs/event for create/delete = " << nsecpercall << std::endl;
      break;

    case HELP:
      std::cout << " Exerciser program for the EM functions.\n";
      std::cout << " Typical sequence:\n";
      std::cout << " create event manager\n";
      std::cout << " printInternals\n";
      std::cout << " get some pages via getPCIAddressOfFreePage\n";
      std::cout << " get the corresponding EDs: getEventDesciptor(PA)\n";
      std::cout << " create event(s)\n";
      std::cout << " check with printInternals that hashing OK\n";
      std::cout << " try getById & deleteById & check with printInternals\n";
      std::cout << " delete event manager\n";
      break;

    case QUIT:
      quit_flag = 1;
      break;

    default:
      std::cout << " not implemented yet\n";
    } /*main switch */
  } while (quit_flag == 0);

  return 0;
}
