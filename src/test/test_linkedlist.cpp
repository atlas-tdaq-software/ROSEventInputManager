/****************************************************************/
/*								*/
/*  file test_linkedlist.c					*/
/*								*/
/*  exerciser for the linked list package			*/
/*								*/
/*  980518  J.O.Petersen EP					*/
/*  980528  adapt to variable list element size			*/
/*								*/
/****************************************************************/


#include <iostream>
#include <iomanip>
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"
#include "ROSEventInputManager/iom_linkedlist.h"

#define NUMBER_OF_ELEMENTS 10

enum 
{
  OPEN = 0x01,
  ADD_FIRST,
  ADD_LAST,
  ADD_ORDERED,
  REMOVE_FIRST,
  REMOVE_BY_KEY,
  REMOVE_BY_ADD,
  FIND,
  PRINTLIST,
  CLOSE,
  PRINTLOCALTABLE,
  HELP,
  QUIT = 100
};


void LL_PrintLocalTable(T_ListHeader* l_l_head);


/************/
int main(void)
/************/ 
{
  char option;
  int quit_flag, key, pagenum, i, j, rem_data[NUMBER_OF_ELEMENTS][3], ext_data[NUMBER_OF_ELEMENTS][3];
  err_type c_stat;

  T_ListHeader l_head;
  T_ListElement el[NUMBER_OF_ELEMENTS];
  T_ListElement *el_ptr;
  T_ListElement l_el;

  quit_flag = 0;

  // initialise linked list head 
  l_head.el_size = sizeof(T_ListElement);
  l_head.el_base = el;
  l_head.fpge = INIT_PAGENUM;
  l_head.lpge = INIT_PAGENUM;
  l_head.nelem = 0;

  // initialise the Elements
  for (i = 0; i < NUMBER_OF_ELEMENTS; i++) 
  {
    el[i].key = NUMBER_OF_ELEMENTS - i;		// opposite sense 
    el[i].next = INIT_PAGENUM;
    el[i].page = i;
    el[i].loc_data = i + 10;
    el[i].rem_data = &rem_data[i][0];
    el[i].ext_data = &ext_data[i][0];
    for (j = 0; j < 3; j++) 
    {
      rem_data[i][j] = 10 * i + j + 1000;
      ext_data[i][j] = 10 * i + j + 2000;
    }
  }

  el_ptr = &l_el;

  do 
  {
    std::cout << "\n\n";
    std::cout << " Open             : " << OPEN << std::endl;
    std::cout << " AddFirst         : " << ADD_FIRST << std::endl;
    std::cout << " AddLast          : " << ADD_LAST << std::endl;
    std::cout << " AddOrdered       : " << ADD_ORDERED << std::endl;
    std::cout << " RemoveFirst      : " << REMOVE_FIRST << std::endl;
    std::cout << " RemoveByKey      : " << REMOVE_BY_KEY << std::endl;
    std::cout << " RemoveByAdd      : " << REMOVE_BY_ADD << std::endl;
    std::cout << " Find             : " << FIND << std::endl;
    std::cout << " PrintList        : " << PRINTLIST << std::endl;
    std::cout << " Close            : " << CLOSE << std::endl;
    std::cout << " PRINTLOCALTABLE  : " << PRINTLOCALTABLE << std::endl;
    std::cout << " HELP             : " << HELP << std::endl;
    std::cout << " QUIT             : " << QUIT << std::endl;

    std::cout << " ? : " ;
    option = getdec(); // get an integer 

    switch(option)   
    {
    case OPEN:
      c_stat = LL_Open();
      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);
      else 
	std::cout << "\n LL_Open OK\n";
      break;

    case ADD_FIRST: 
      std::cout << " page (EL) to add (<" << NUMBER_OF_ELEMENTS << ") = ";
      pagenum = getdec();

      c_stat = LL_AddFirst(&l_head, &el[pagenum]);

      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);      
      else 
      {
	std::cout << " LL_AddFirst OK\n";
	std::cout << " added page " << pagenum << "  @ " << HEX(&el[pagenum]) << " as first in list\n";
      }
      break;

    case ADD_LAST: 
      std::cout << " page (EL) to add (<" << NUMBER_OF_ELEMENTS << ") = ";
      pagenum = getdec();

      c_stat = LL_AddLast(&l_head, &el[pagenum]);

      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);
      else 
      {
	std::cout << " LL_AddLast OK\n";
	std::cout << " added page " << pagenum << "  @ " << HEX(&el[pagenum]) << " as last in list\n";
      }
      break;

    case ADD_ORDERED: 
      std::cout << " page (EL) to add (<" << NUMBER_OF_ELEMENTS << ") = ";
      pagenum = getdec();

      c_stat = LL_AddOrdered(&l_head, &el[pagenum]);
      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);
      else 
      {
	std::cout << " LL_AddOrdered OK\n";
	std::cout << " added page " << pagenum << " @ " << HEX(&el[pagenum]) << " to the list\n ";
      }
      break;

    case REMOVE_FIRST: 
      c_stat = LL_RemoveFirst(&l_head, &el_ptr);

      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);
      else 
      {
	std::cout << " LL_RemoveFirst  OK\n";
	std::cout << " removed page " << el_ptr->page << " @ " << HEX(el_ptr) << " with key = " << el_ptr->key << " in list\n ";
      }
      break;

    case REMOVE_BY_KEY: 
      std::cout << " key to remove = ";
      key = getdec();

      c_stat = LL_RemoveByKey(&l_head, key, &el_ptr);

      if (c_stat == LLE_OK) 
      {
	std::cout << " LL_RemoveByKey  OK\n";
	std::cout << " removed page " << el_ptr->page << " @ " << HEX(el_ptr) <<  " with key = " << el_ptr->key << " in list\n ";
      }
      else 
	rcc_error_print(stdout, c_stat);

      break;

    case REMOVE_BY_ADD: 
      std::cout << " page (EL) to remove (<" << NUMBER_OF_ELEMENTS << ") = ";
      pagenum = getdec();

      std::cout << " element is @ " << HEX(&el[pagenum]) << std::endl;
      c_stat = LL_RemoveByAdd(&l_head, &el[pagenum]);

      if (c_stat == LLE_OK) 
      {
	std::cout << " LL_RemoveByAdd  OK\n";
	std::cout << " removed page # " << el[pagenum].page << " @ " << HEX(&el[pagenum]) << " with key = " << el[pagenum].key << " in list\n ";
      }
      else 
	rcc_error_print(stdout,c_stat);
      break;

    case FIND: 
      std::cout << " FIND key = ";
      key = getdec();

      c_stat = LL_Find(&l_head, key,&el_ptr);

      if (c_stat == LLE_OK) 
      {
	std::cout << " LL_Find  OK\n";
	std::cout << " found page # " << el_ptr->page << " @ " << HEX(el_ptr) << " with key = " << el_ptr->key << " in list\n ";
      }
      else 
	rcc_error_print(stdout, c_stat);
      break;

    case PRINTLIST: 
      c_stat = LL_PrintList(&l_head);

      if (c_stat == LLE_OK) 
	std::cout << " LL_PrintList OK\n";
      else 
	rcc_error_print(stdout, c_stat);
      break;

    case CLOSE:
      c_stat = LL_Close();
      if (c_stat != LLE_OK) 
	rcc_error_print(stdout, c_stat);
      else 
	std::cout << "\n LL_Close OK\n";
      break;

    case PRINTLOCALTABLE:
      LL_PrintLocalTable(&l_head);
      break;

    case HELP:
      std::cout << " Exerciser program for the Low Level Linked List functions.\n";
      std::cout << " The program builds a pool of list elements in the LOCAL\n";
      std::cout << " element table.\n";
      std::cout << " These elements can be manipulated by the basic LL functions \n";
      std::cout << " i.e. added to the list, removed from the list etc.\n";
      std::cout << " It is the user's responsability to keep track of the elements\n";
      std::cout << " in the local table. This can be done via the command \n";
      std::cout << " PRINTLOCALTABLE\n";
      std::cout << " This command prints the (whole) local table of elements\n";
      std::cout << " All the other commands correspond one-to-one with functions\n";
      std::cout << " in the LL library.\n";
      break;

    case QUIT:
      quit_flag = 1;
      break;

    default:
      std::cout << " not implemented yet\n";
    } /*main switch */
  } while (quit_flag == 0);

  return 0;
}


/*********************************************/
void LL_PrintLocalTable(T_ListHeader *l_l_head)
/*********************************************/
{
  int i;
  T_ListElement *el_ptr;

  std::cout << std::endl;
  std::cout << " local EL table  - including elements on the list:\n";

  el_ptr = l_l_head->el_base;

  std::cout << " element #   key  page  next  loc_data         rem_data              ext_data\n";
  for (i = 0; i < NUMBER_OF_ELEMENTS; i++) 
  {
    std::cout << std::setw(10) << i << std::setw(6) << el_ptr->key
         << std::setw(6) << el_ptr->page << std::setw(6) << el_ptr->next
         << std::setw(10) << el_ptr->loc_data
         << "  " << HEX(el_ptr->rem_data)
         << " : " << std::setw(8) << *((int*)(el_ptr->rem_data))
         << "  " << HEX(el_ptr->ext_data)
         << " : " << std::setw(8) << *((int*)(el_ptr->ext_data)) << std::endl;
     
    el_ptr = l_l_head->el_base+i+1;
  }
}              
