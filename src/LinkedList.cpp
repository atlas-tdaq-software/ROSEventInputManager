/************************************************************************/
/*									*/
/*  file : LinkedList.cpp						*/
/*									*/
/*  Generic Linked List functions for ATLAS TDAQ S/W		        */
/*									*/
/*  Functions for handling a singly linked list of Objects     	        */
/*									*/
/*  These functions allow to implement 				        */
/*  queues (LIFOs) :							*/
/*    add to rear - remove from front					*/
/*  stacks (FIFOs) :							*/
/*    add to front - remove from front					*/
/*  unordered lists 							*/
/*    add to rear or front - remove by searching for element		*/
/*									*/
/*  Note on "list overflows" : this is the user's responsability	*/
/*  the memory allocation is done elsewhere				*/
/*									*/
/*  980517  J.Petersen EP						*/
/*									*/
/************************************************************************/

#include <iostream>
#include <iomanip>
#include <string.h>
#include "DFDebug/DFDebug.h"
#include "ROSEventInputManager/iom_linkedlist.h"


// global def of the error function 
static err_type ll_err_get(err_pack err, err_str pid, err_str code);
 
static int is_open = 0;


/********************/
err_type LL_Open(void) 
/********************/
{
  int c_stat;
 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Open called");
  if (is_open)  //  multiple opens 
  {
    is_open++;
    DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Open done");
    return(RCC_ERROR_RETURN(0, LLE_ISOPEN));
  }
 
  c_stat = rcc_error_init(P_ID_LL_RCC, ll_err_get);
  if(c_stat != 0) 
    return(RCC_ERROR_RETURN(0, LLE_ERROR_FAIL));
 
  is_open = 1; 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Open done");
  return(RCC_ERROR_RETURN(0, LLE_OK));
}


/*********************/
err_type LL_Close(void) 
/*********************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Close called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));

  if (is_open == 1)  // do the close 
    is_open = 0;
  else               // dummy close
    is_open--;
 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Close done");
  return(RCC_ERROR_RETURN(0, LLE_OK)); 
}


// add an element to the front of the Linked List
/*************************************************************/
err_type LL_AddFirst(T_ListHeader *l_l_head, T_ListElement *el)
/*************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_AddFirst called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));

  if (l_l_head->nelem == 0) 
  {	// list empty
    el->next = INIT_PAGENUM;
    l_l_head->fpge = el->page;
    l_l_head->lpge = el->page;
    l_l_head->nelem = 1;
  }
  else 
  { 				// don't touch last
    el->next = l_l_head->fpge;
    l_l_head->fpge = el->page;
    l_l_head->nelem++;
  }

  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_AddFirst done");
  return(RCC_ERROR_RETURN(0, LLE_OK));
}


/************************************************************/
err_type LL_AddLast(T_ListHeader *l_l_head, T_ListElement *el)
/************************************************************/
{
  T_ListElement *l_el;  

  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_AddLast called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));
 
  if (l_l_head->nelem == 0) 
  {        // list empty
    el->next = INIT_PAGENUM;
    l_l_head->fpge = el->page;
    l_l_head->lpge = el->page;
    l_l_head->nelem = 1;
  }
  else  // don't touch first
  {                               
    el->next = INIT_PAGENUM;
    l_el = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->lpge * l_l_head->el_size);
    l_el->next = el->page;
    l_l_head->lpge = el->page;
    l_l_head->nelem++;
  }
 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_AddLast done");
  return(RCC_ERROR_RETURN(0, LLE_OK));
}


//This function is currently not used but we have decided to leave it in the package as
//Giovanna may want to use it in the future.......
/***************************************************************/
err_type LL_AddOrdered(T_ListHeader *l_l_head, T_ListElement *el)  
/***************************************************************/
{
  T_ListElement *cur;
  T_ListElement *prev;
  int in_key;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_AddOrdered called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));

  if (l_l_head->nelem == 0) 
  {        // list empty
    el->next = INIT_PAGENUM;
    l_l_head->fpge = el->page;
    l_l_head->lpge = el->page;
    l_l_head->nelem = 1;
    return(RCC_ERROR_RETURN(0, LLE_OK));
  }

  in_key = el->key;

  // statistically, key is often the last?
  cur = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->lpge * l_l_head->el_size);
  if (in_key > cur->key)
  {			// insert as last
    el->next = INIT_PAGENUM;
    cur->next = el->page;
    l_l_head->lpge = el->page;
    l_l_head->nelem++;
    return(RCC_ERROR_RETURN(0, LLE_OK));
  }

  cur = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);

  if (in_key < cur->key) 
  {			// insert as first
    el->next = l_l_head->fpge;
    l_l_head->fpge = el->page;
    l_l_head->nelem++;
    return(RCC_ERROR_RETURN(0, LLE_OK));
  }

  do 
  {
    prev = cur;
    cur = (T_ListElement*) ((char*)(l_l_head->el_base) + cur->next * l_l_head->el_size);  // start with second element
  } while (cur->key < in_key);

  el->next = prev->next;
  prev->next = el->page;
  l_l_head->nelem++;    
  return(RCC_ERROR_RETURN(0, LLE_OK));
}


/*********************************************************************/
err_type LL_RemoveFirst(T_ListHeader *l_l_head, T_ListElement** el_ptr)
/*********************************************************************/
{
  T_ListElement *l_el;
  
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveFirst called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));
 
  // Linked List is empty
  if (l_l_head->nelem == 0)
    return(RCC_ERROR_RETURN(0, LLE_EMPTYLIST));

  l_el = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);
 
  *el_ptr = l_el;                       // the element we remove
 
  l_l_head->fpge = l_el->next;
  l_el->next = INIT_PAGENUM;
  l_l_head->nelem--;
 
  if (l_l_head->nelem == 0) 
    l_l_head->lpge = INIT_PAGENUM;      // if list becomes empty
 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveFirst done");
  return(RCC_ERROR_RETURN(0, LLE_OK)); 
}


/******************************************************************************/
err_type LL_RemoveByKey(T_ListHeader *l_l_head, int key, T_ListElement** el_ptr)
/******************************************************************************/
{
  T_ListElement *cur;
  T_ListElement *prev;
 
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveByKey called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));
 
  // Linked List is empty
  if (l_l_head->nelem == 0)
    return(RCC_ERROR_RETURN(0, LLE_EMPTYLIST));
 
  cur = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);
  prev = NULL;

  while(1) 
  {
    if (cur->key == key)  // found 
    {			
      *el_ptr = cur;
      if (prev == NULL) // first in list
      {
        if ((l_l_head->fpge = cur->next) == INIT_PAGENUM) 
          l_l_head->lpge = INIT_PAGENUM;        // if last element removed
      }  
      else // not first in list
      { 
        prev->next = cur->next;
        if (cur->next == INIT_PAGENUM) // if last element removed
          l_l_head->lpge = prev->page;
      }

      cur->next = INIT_PAGENUM;
      l_l_head->nelem--;
      DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveByKey done");
      return(RCC_ERROR_RETURN(0, LLE_OK));
    }
    else // not found yet
    {	
      if (cur->next == INIT_PAGENUM) // end of list and not found
        return(RCC_ERROR_RETURN(0, LLE_NOTFOUND));
      prev = cur;
      cur = (T_ListElement*) ((char*)(l_l_head->el_base) + cur->next * l_l_head->el_size);
    }
  }
}


/****************************************************************/
err_type LL_RemoveByAdd(T_ListHeader *l_l_head, T_ListElement *el)
/****************************************************************/
{
  T_ListElement *cur;
  T_ListElement *prev;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveByAdd called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));

  // Linked List is empty
  if (l_l_head->nelem == 0)
    return(RCC_ERROR_RETURN(0, LLE_EMPTYLIST));
 
  cur = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);
  prev = NULL;

  while(1) 
  {
    if (cur == el) 
    {				                // found
      if (prev == NULL) 
      {			                        // first in list
        if ((l_l_head->fpge = cur->next) == INIT_PAGENUM) 
          l_l_head->lpge = INIT_PAGENUM;        // if last element removed
      }
      else 
      { // not first in list
        prev->next = cur->next;
        if (cur->next == INIT_PAGENUM)          // if last element removed
          l_l_head->lpge = prev->page;
      }

      cur->next = INIT_PAGENUM;
      l_l_head->nelem--;
      DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_RemoveByAdd done");
      return(RCC_ERROR_RETURN(0, LLE_OK));
    }
    else 
    {	// not found yet 
      if (cur->next == INIT_PAGENUM)            // end of list and not found
        return(RCC_ERROR_RETURN(0, LLE_NOTFOUND));

      prev = cur;
      cur = (T_ListElement*)
      ((char*)(l_l_head->el_base) + cur->next * l_l_head->el_size);
    }
  }
}


/***********************************************************************/
err_type LL_Find(T_ListHeader *l_l_head, int key, T_ListElement** el_ptr)
/***********************************************************************/
{
  T_ListElement *cur;
  
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Find called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));
 
  // Linked List is empty 
  if (l_l_head->nelem == 0) 
    return(RCC_ERROR_RETURN(0, LLE_EMPTYLIST));
 
  cur = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);
 
  while(1) 
  {
    if (cur->key == key)
    {			// found
      *el_ptr = cur;
      DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_Find done");
      return(RCC_ERROR_RETURN(0, LLE_OK));
    }
    else 
    {
      if (cur->next == INIT_PAGENUM) // EOL and key not found
        return(RCC_ERROR_RETURN(0, LLE_NOTFOUND));

      cur = (T_ListElement*) ((char*)(l_l_head->el_base) + cur->next * l_l_head->el_size);
    }
  }
}


/*******************************************/
err_type LL_PrintList(T_ListHeader *l_l_head)
/*******************************************/
{
  int i;
  T_ListElement *el_ptr;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_PrintList called");
  if (!is_open)  
    return(RCC_ERROR_RETURN(0, LLE_ISNOTOPEN));

  // Linked List is empty
  if (l_l_head->nelem == 0) 
    return(RCC_ERROR_RETURN(0, LLE_EMPTYLIST));

  std::cout << " List Header :" << std::endl;
  std::cout << " EL base address = " << HEX(l_l_head->el_base) << std::endl;
  std::cout << " EL size         = " << l_l_head->el_size << std::endl;
  std::cout << " first page      = " << l_l_head->fpge << std::endl;
  std::cout << " last  page      = " << l_l_head->lpge << std::endl;
  std::cout << " # elements      = " << l_l_head->nelem << std::endl;

  el_ptr = (T_ListElement*) ((char*)(l_l_head->el_base) + l_l_head->fpge * l_l_head->el_size);
  std::cout << std::endl;
  std::cout << "     key  page #    next    loc_data    rem_data    ext_data\n";
  
  for (i = 0; i < l_l_head->nelem; i++) 
  {
    std::cout << std::setw(8) << el_ptr->key 
              << std::setw(8) << el_ptr->page
              << std::setw(8) << el_ptr->next 
	      << "           " << el_ptr->loc_data
              << "  " << HEX(el_ptr->rem_data) 
	      << "  " << HEX(el_ptr->ext_data) 
	      << std::endl;
    el_ptr = (T_ListElement*) ((char*)(l_l_head->el_base) + el_ptr->next * l_l_head->el_size);
  }
  std::cout << std::endl;
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::LL_PrintList done");
  return(RCC_ERROR_RETURN(0, LLE_OK));
}


/**********************************************************/
err_type ll_err_get(err_pack err, err_str pid, err_str code)
/**********************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "LinkedList::ll_err_get called");
  strcpy(pid, P_ID_LL_RCC_STR);
 
  switch(RCC_ERROR_MINOR(err))
  {
    case LLE_OK:        strcpy(code, LLE_OK_STR);        break;
    case LLE_ISOPEN:    strcpy(code, LLE_ISOPEN_STR);    break;
    case LLE_ISNOTOPEN: strcpy(code, LLE_ISNOTOPEN_STR); break;
    case LLE_EMPTYLIST: strcpy(code, LLE_EMPTYLIST_STR); break;
    case LLE_NOTFOUND:  strcpy(code, LLE_NOTFOUND_STR);  break;
    default:            strcpy(code, LLE_NO_CODE_STR);
                        return(RCC_ERROR_RETURN(0, LLE_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, LLE_OK));
}

