// $Id$
/*
  ATLAS ROS Software

  Class: EventInputManager 

  Authors: ATLAS ROS group
*/

#include <iostream>
#include <iomanip>
#include <string.h>
#include <vector>
#include <sys/types.h>
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSEventInputManager/EventInputManager.h"

using namespace ROS;

/***********************************************************************************************************************/
EventInputManager::EventInputManager(int noPages, int pageSize, int numberOfHashLists, int pooltype, int noPagesReserved)
/***********************************************************************************************************************/
{
  err_type ret;
  err_str rcc_err_str;
  u_long vaLc, paLc, vaEd, paEd;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::creator called");
  DEBUG_TEXT(DFDB_ROSEIM, 20, "EventInputManager::creator  noPages           = " << noPages);
  DEBUG_TEXT(DFDB_ROSEIM, 20, "EventInputManager::creator  pageSize          = " << pageSize);
  DEBUG_TEXT(DFDB_ROSEIM, 20, "EventInputManager::creator  numberOfHashLists = " << numberOfHashLists);
  DEBUG_TEXT(DFDB_ROSEIM, 20, "EventInputManager::creator  pooltype          = " << pooltype);
  DEBUG_TEXT(DFDB_ROSEIM, 20, "EventInputManager::creator  noPagesReserved   = " << noPagesReserved);

  // Linked List
  ret = LL_Open();
  if(ret != LLE_OK && ret != LLE_ISOPEN) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  m_memoryPool = new MemoryPool_CMEM(noPages, pageSize, pooltype, noPagesReserved);
  m_numberOfPages = noPages;
  m_pageSize = pageSize;
  m_poolBaseAdd = m_memoryPool->getPhysicalAddress(0);	// page 0
  m_numberOfHashLists = numberOfHashLists;

  // numberOfHashLists MUST be equal to 2**N
  int hashOK = 0;
  for (int i = 0; i < 31; i++) 
    if (numberOfHashLists == (1 << i)) 
      hashOK = 1;

  if (hashOK == 0) 
  {
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, ILLEGALNUMBEROFHASHLISTS, "");
    throw(ex1);
  }

  // compute the mask for Local ID computation = 2**N - 1 !!
  m_modMask = numberOfHashLists - 1;

  ret = CMEM_Open();
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentAllocate(numberOfHashLists * (int)sizeof(T_ListHeader), "HASHTABLE", &m_lcHandle);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentVirtualAddress(m_lcHandle, &vaLc);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentPhysicalAddress(m_lcHandle, &paLc);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  m_lcBase = (T_ListHeader*)vaLc;

  // construct the array of event descriptors
  ret = CMEM_SegmentAllocate(m_numberOfPages * (int)sizeof(evDesc_t), "EDTABLE", &m_edHandle);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentVirtualAddress(m_edHandle, &vaEd);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentPhysicalAddress(m_edHandle, &paEd);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  m_edBase = (evDesc_t*)vaEd;
  m_lastId = 0;                 //In fact -1 would be more correct but 0 works as well because this variable is only used to implement a "lost fragment" logic
  InitClassLists();
  InitEventDesc();
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::creator done");
}


/*************************************/
EventInputManager::~EventInputManager()
/*************************************/
{
  err_type ret;
  err_str rcc_err_str;
  
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::destructor called");

  ret = CMEM_SegmentFree(m_edHandle);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_SegmentFree(m_lcHandle);
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  ret = CMEM_Close();
  if (ret != CMEM_RCC_SUCCESS) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  // Hash Lists
  ret = LL_Close();
  if(ret != LLE_OK) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, EXT_ERROR, rcc_err_str);
    throw(ex1);
  }

  delete m_memoryPool;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::destructor done");
}


//Called by the SequentialDataChannel
/*********************************/
void EventInputManager::reset(void)
/*********************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::reset called");
  m_memoryPool->reset();
  m_lastId = 0;
  InitClassLists();
  InitEventDesc();
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::reset done");
}


/**********************************************/
u_int EventInputManager::numberOfFreePages(void)
/**********************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::numberOfFreePages called");
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::numberOfFreePages done");
  return (m_memoryPool->numberOfFreePages());
}


/*****************************************************/
u_long EventInputManager::getPCIAddressOfFreePage(void)
/*****************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getPCIAddressOfFreePage called");
  MemoryPage *memPage = m_memoryPool->getPage();  
  //We don't have the check the success of getPage. If no pages are available getPage will throw an exception that 
  //we will catch at a higher level. Therefor we can assume that we got a page and continue with:
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getPCIAddressOfFreePage done");
  return (memPage->physicalAddress());
}


/************************************************/
MemoryPool *EventInputManager::getMemoryPool(void)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getMemoryPool called");
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getMemoryPool done");
  return m_memoryPool;
}


/***************************************************************/
evDesc_t *EventInputManager::getEventDescriptor(MemoryPage *memP)
/***************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptor(1) called");
  int pageNo = memP->pageNumber();
  evDesc_t *edVa = m_edBase + pageNo;	// ptr arithmetic ..
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptor(1) done");
  return edVa;
}


/**************************************************************/
evDesc_t *EventInputManager::getEventDescriptor(u_long physAddr)
/**************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptor(2) called");
  int pageNo = (physAddr - m_poolBaseAdd) / m_pageSize;
  evDesc_t *edVa = m_edBase + pageNo;	// ptr arithmetic ..
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptor(2) done");
  return edVa;
}


/***********************************************/
void EventInputManager::createEvent(evDesc_t *ed)
/***********************************************/
{
  err_type ret;
  err_str rcc_err_str;
  T_ListHeader *l_l_head;  

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::createEvent called");
  int lid = getLid(ed->L1id);
  ed->lid = lid;                                // remember it

  l_l_head = m_lcBase + lid;                    // -> the list header
  ret = LL_AddLast(l_l_head, (T_ListElement*)ed);
  if (ret != LLE_OK) 
  {
    rcc_error_string(rcc_err_str, ret);
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, ADDLINKEDLIST, rcc_err_str);
    throw(ex1);
  }

  // m_lastId can only increase: protect against events being re-created (since lost ..)  
  //MJ: This logic breaks down when the L1IDs wrap round. Document it!
  if (ed->L1id > m_lastId) 
    m_lastId = ed->L1id;	// record last L1id

  DEBUG_TEXT(DFDB_ROSEIM, 10, "EventInputManager::createEvent  L1ID = " << ed->L1id << "  m_lastId = " << m_lastId);
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::createEvent done");
}


/********************************************/
evDesc_t *EventInputManager::getById(int L1id)
/********************************************/
{
  err_type ret;
  T_ListHeader *l_l_head;
  evDesc_t *ed_ptr;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getById called");
  int lid = getLid(L1id);
  l_l_head = m_lcBase + lid;               // -> the list header

  ret = LL_Find(l_l_head, L1id, (T_ListElement**)&ed_ptr);
  if (ret == LLE_OK)  // event found 
  {
    DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getById done");
    return ed_ptr;
  }
  else                // first event cannot return EIM_NEVERTOCOME ..
  {
    if ((u_int)L1id <= m_lastId && m_lastId != 0) // will never come
    {	
      DEBUG_TEXT(DFDB_ROSEIM, 5, "EventInputManager::getById  L1ID = " << L1id << ",  m_lastId = " << m_lastId);
      return (evDesc_t *)EIM_NEVERTOCOME;
    }
    else // may come
      return (evDesc_t *)EIM_MAYCOME;
  }
}


/**********************************************/
void EventInputManager::deleteById(int level1Id)
/**********************************************/
{
  evDesc_t *EvDesc_out;
  err_str rcc_err_str;
  T_ListHeader *l_l_head;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::deleteById called");
  DEBUG_TEXT(DFDB_ROSEIM, 10, "EventInputManager::deleteById level1Id = " << level1Id);
  int lid = getLid(level1Id);
  l_l_head = m_lcBase + lid;

  err_type ret = LL_RemoveByKey(l_l_head, level1Id, (T_ListElement**)&EvDesc_out);
  //The HW Robin may return the L1IDs of fragments it could not delete. As we are here in the context of a
  //DDT it makes not sense to wait for fragments that are arriving late. If a L1ID cannot be found by LL_RemoveByKey
  //it makes no sense to repeat (or re-queue) the call.  
  if (ret == LLE_OK) 
  {
    // delete the corresponding memory page
    MemoryPage *memPage = EvDesc_out->myPage;
    memPage->free();   
  }
  else   // we leave the exception in until further notice
  {
    rcc_error_string(rcc_err_str, ret);
    //only notify a warning, but try to go on
    CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, REMOVELINKEDLIST, "\n Cannot delete L1ID = " << level1Id << "  " << rcc_err_str);
    ers::error(ex1);
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::deleteById done");
}


/*********************************************************************/
void EventInputManager::deleteById(const std::vector<u_int>* level1Ids)
/*********************************************************************/
{
  evDesc_t *EvDesc_out;
  err_str rcc_err_str;
  T_ListHeader *l_l_head;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::deleteById(group) called");
  for (std::vector<u_int>::const_iterator level1Id = level1Ids->begin(); level1Id != level1Ids->end(); level1Id++) 
  {
    u_int ev_id = *level1Id;
    DEBUG_TEXT(DFDB_ROSEIM, 10, "EventInputManager::deleteById: L1ID = " << ev_id);
    int lid = getLid(ev_id);
    l_l_head = m_lcBase + lid;
    err_type ret = LL_RemoveByKey(l_l_head, ev_id, (T_ListElement**)&EvDesc_out);
    if (ret == LLE_OK) 
    {
      // delete the corresponding memory page
      MemoryPage *memPage = EvDesc_out->myPage;
      memPage->free();  
    }
    else 
    {
      rcc_error_string(rcc_err_str, ret);
      //only notify a warning, but try to go on
      CREATE_ROS_EXCEPTION(ex1, EventInputManagerException, REMOVELINKEDLIST, "\n Cannot delete L1ID = " << ev_id << "  " << rcc_err_str);
      ers::error(ex1);
    }
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::deleteById(group) done");
}


/******************************************/
void EventInputManager::printInternals(void)
/******************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::printInternals called");
  dumpHashTable(0, m_numberOfHashLists - 1);
  dumpEDVector();
  m_memoryPool->dumpVector();
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::printInternals done");
}


/*******************************************************/
evDesc_t *EventInputManager::getEventDescriptorBase(void)
/*******************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptorBase called");
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getEventDescriptorBase done");
  return(m_edBase);
}


/************************************************/
T_ListHeader *EventInputManager::getHashBase(void)
/************************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getHashBase called");
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getHashBase done");
  return(m_lcBase);
}


/***********************************************/
int EventInputManager::getNumberOfHashLists(void)
/***********************************************/
{
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getNumberOfHashLists called");
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::getNumberOfHashLists done");
  return (m_numberOfHashLists);
}


/********************************************************/
void EventInputManager::dumpHashTable(int First, int Last)
/********************************************************/
{
  T_ListHeader *l_head_ptr;
  evDesc_t *l_ed;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::dumpHashTable called");

  l_head_ptr = m_lcBase;
  std::cout << " Hash Table @ " << m_lcBase << std::endl;
  std::cout << " Bucket #  First Pno   Last Pno  #evs  L1id's\n";
  for (int i = First; i <= Last; i++) 
  {
    std::cout << std::setw(9) << i << std::setw(11) << l_head_ptr->fpge << std::setw(11) << l_head_ptr->lpge << std::setw(6) << l_head_ptr->nelem;
    if (l_head_ptr->nelem > 0) 
    {
      l_ed = (evDesc_t*)(l_head_ptr->el_base) + l_head_ptr->fpge;
      for (int j = 0; j < l_head_ptr->nelem; j++) 
      {
        std::cout << std::setw(10) << ((T_ListElement*)l_ed)->key;
        l_ed = (evDesc_t*)(l_head_ptr->el_base) + ((T_ListElement*)l_ed)->next;
      }
    }
    std::cout << std::endl;
    l_head_ptr++;
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::dumpHashTable done");
}


/************************************/
void EventInputManager::dumpEDVector()
/************************************/
{
  evDesc_t* ed_ptr;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::dumpEDVector called");

  ed_ptr = m_edBase;

  std::cout << " ED Table @ " << m_edBase << std::endl;
  std::cout << " Index    ED pointer  L1id  pageno      next  lid      pgAdd  refCnt  ROD: size  status\n";

  for (int i = 0; i < m_numberOfPages; i++) 
  {
    std::cout << std::setw(6) << i << "    " << ed_ptr << std::setw(6) << ed_ptr->L1id << std::setw(8) << ed_ptr->pagenum <<
            std::setw(10) << std::dec << ed_ptr->next << std::setw(5) << ed_ptr->lid <<
            "  " << ed_ptr->myPage << std::setw(8) << ed_ptr->myPage->referenceCount() <<
            std::setw(11) << ed_ptr->RODFragmentSize << std::setw(8) << ed_ptr->RODFragmentStatus << std::endl;
    ed_ptr++;
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::dumpEDVector done");
}


/**************************************/
void EventInputManager::InitClassLists()
/**************************************/
{
  T_ListHeader *l_head_ptr;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::InitClassLists called");

  l_head_ptr = m_lcBase;

  for (int i = 0; i < m_numberOfHashLists; i++) 
  {
    l_head_ptr->el_base = (T_ListElement*)m_edBase;
    l_head_ptr->el_size = sizeof(evDesc_t);
    l_head_ptr->fpge = INIT_PAGENUM;
    l_head_ptr->lpge = INIT_PAGENUM;
    l_head_ptr->nelem = 0;
    l_head_ptr++;
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::InitClassLists done");
}


/*************************************/
void EventInputManager::InitEventDesc()
/*************************************/
{
  evDesc_t *ed_ptr;

  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::InitEventDesc called");

  ed_ptr = m_edBase;
  for (int i = 0; i < m_numberOfPages; i++) 
  {
    ed_ptr->L1id = i;
    ed_ptr->pagenum = i;
    ed_ptr->next = INIT_PAGENUM;
    ed_ptr->lid = 0;
    ed_ptr->myPage = m_memoryPool->getPagePointerInitial(i);
    ed_ptr->RODFragmentSize = 0;
    ed_ptr->RODFragmentStatus = 0;
    ed_ptr++;
  }
  DEBUG_TEXT(DFDB_ROSEIM, 15, "EventInputManager::InitEventDesc done");
}


