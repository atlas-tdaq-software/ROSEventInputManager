// -*- c++ -*-
/*
  ATLAS EVENTINPUTMANAGER Software

  Class: EVENTINPUTMANAGEREXCEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef EVENTINPUTMANAGEREXCEPTION_H
#define EVENTINPUTMANAGEREXCEPTION_H

#include <sys/types.h>
#include <string>
#include <iostream>
#include "DFExceptions/ROSException.h"

class EventInputManagerException : public ROSException 
{
  
public:
  enum ErrorCode 
  { 
    ILLEGALNUMBEROFHASHLISTS,
    ADDLINKEDLIST,
    FINDLINKEDLIST,
    REMOVELINKEDLIST,
    EXT_ERROR
  };   

  EventInputManagerException(ErrorCode error);
  EventInputManagerException(ErrorCode error, std::string description);
  EventInputManagerException(ErrorCode error, const ers::Context& context);
  EventInputManagerException(ErrorCode error, std::string description, const ers::Context& context);
  EventInputManagerException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

  virtual ~EventInputManagerException() throw() {};

protected:
  virtual std::string getErrorString(u_int errorId) const;
  virtual ers::Issue * clone() const { return new EventInputManagerException( *this ); }
  static const char * get_uid() {return "ROS::EventInputManagerException";}
  virtual const char* get_class_name() const {return get_uid();}
};


/********************************************************************************************************/
inline EventInputManagerException::EventInputManagerException(EventInputManagerException::ErrorCode error) 
  : ROSException("EventInputManager", error, getErrorString(error)) 
/********************************************************************************************************/
{}


/********************************************************************************************************/
inline EventInputManagerException::EventInputManagerException(EventInputManagerException::ErrorCode error, 
                                                              std::string description) 
  : ROSException("EventInputManager", error, getErrorString(error), description) 
/********************************************************************************************************/
{}


/********************************************************************************************************/
inline EventInputManagerException::EventInputManagerException(EventInputManagerException::ErrorCode error, 
                                                              const ers::Context& context)
  : ROSException("EventInputManager", error, getErrorString(error), context) 
/********************************************************************************************************/
{}


/*****************************************************************************************************************/
inline EventInputManagerException::EventInputManagerException(EventInputManagerException::ErrorCode error, 
                                                              std::string description, const ers::Context& context)
  : ROSException("EventInputManager", error, getErrorString(error), description, context) 
/*****************************************************************************************************************/
{}


/*****************************************************************************************************************/
inline EventInputManagerException::EventInputManagerException(const std::exception& cause, ErrorCode error, 
                                                              std::string description, const ers::Context& context) 
     : ROSException(cause, "EventInputManager", error, getErrorString(error), description, context) 
/*****************************************************************************************************************/
{}

/********************************************************************************/
inline std::string EventInputManagerException::getErrorString(u_int errorId) const 
/********************************************************************************/
{
  std::string rc;    
  switch (errorId) 
  {  
  case ILLEGALNUMBEROFHASHLISTS:
    rc = "Illegal number of hash lists";
    break;
  case ADDLINKEDLIST:
    rc = "Error when adding to Linked List";
    break;
  case FINDLINKEDLIST:
    rc = "Error in Find Linked List";
    break;
  case REMOVELINKEDLIST:
    rc = "Error when removing from Linked List";
    break;
  case EXT_ERROR:
    rc = "External error";
    break;
  default:
    rc = "";
    break;
  }
  return rc;
}

#endif //EVENTINPUTMANAGEREXCEPTION_H
 
