// $Id$
/*
  file EventInputManager.h

  The Event Input Manager for the Software RobIn

  Class: EventInputManager 
  Authors: J.O.Petersen ATD 

*/

#ifndef _EVENTINPUTMANAGER_H
#define _EVENTINPUTMANAGER_H

#include <sys/types.h>
#include "ROSMemoryPool/MemoryPool.h"
#include "EventDescriptor.h"
#include "iom_linkedlist.h"
#include "EventInputManagerException.h"

namespace ROS
{
  class EventInputManager 
  {
    public:

    enum 
    {
     EIM_NEVERTOCOME = -1,
     EIM_MAYCOME  =  0
    };

    EventInputManager(int noPages, int pageSize, int numberOfHashLists,
                      int pooltype = MemoryPool::ROSMEMORYPOOL_CMEM_GFP, int noPagesReserved = 0);

    ~EventInputManager();

    void reset(void);
    u_long getPCIAddressOfFreePage(void);
    u_int numberOfFreePages(void);
    evDesc_t *getEventDescriptor(MemoryPage *memP);
    evDesc_t *getEventDescriptor(u_long physAddr);
    void createEvent(evDesc_t *ed);
    evDesc_t *getById(int L1id);
    void deleteById(int level1Id);
    void deleteById(const std::vector<u_int> *level1Ids);
    void printInternals (void);
    MemoryPool *getMemoryPool(void);
    evDesc_t *getEventDescriptorBase (void);
    T_ListHeader *getHashBase (void);
    int getNumberOfHashLists(void);

    private:

    inline int getLid (int gid);
    void InitClassLists(void);
    void InitEventDesc(void);
    void dumpHashTable(int First, int Last);
    void dumpEDVector();

    MemoryPool *m_memoryPool;	// the associated memory pool & its parameters
    int m_numberOfPages;
    int m_pageSize;
    u_long m_poolBaseAdd;
    int m_numberOfHashLists;
    evDesc_t *m_edBase;		// VA of the Event Descriptor table
    T_ListHeader *m_lcBase;	// VA of the hash table
    int m_lcHandle;		// CMEM handle to hash table
    int m_edHandle;		// CMEM handle to event descriptor table
    u_int m_modMask;	        // for fast calculation of hash function
    u_int m_lastId;	        // last L1id created
  };

  inline int EventInputManager::getLid (int gid)
  {
    return (gid & m_modMask);
  }
}	// ROS space

#endif

