/*
  file EventDescriptor.h

  The Event Descriptor for the Software RobIn

  Class: EventDescriptor 
  Authors: ATLAS ROS group

*/

#ifndef EVENTDESCRIPTOR
#define EVENTDESCRIPTOR

#include <sys/types.h>
#include "ROSMemoryPool/MemoryPage.h"

namespace ROS 
{
  typedef struct EvDesc *ed_ptr;
  typedef struct EvDesc 
  {
    u_int L1id;			// the Level 1 ID
    int pagenum;		// memory page number associated with the ED
    int next;			// link field in the hash list for lid
    u_int lid;			// the hash table index
    MemoryPage *myPage;		// virtual address of the corresponding memory page (object)
    u_int RODFragmentSize;
    u_int RODFragmentStatus;
  } evDesc_t;
}

#endif

