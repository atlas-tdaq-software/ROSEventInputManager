/****************************************************************/
/*  file iom_linkedlist.h					*/
/*								*/
/*  definitions for the LOW LEVEL IOM Linked List functions	*/
/*								*/
/*  960820   J.Petersen ECP					*/
/*  980330   restructure iomdaq.h, create this file             */
/*  980405   adapt to new EM API		                */
/*								*/
/*  980518   new generic Low Level Linked List Package		*/
/*								*/
/****************************************************************/

#ifndef _IOMLL_H
#define _IOMLL_H

#include "rcc_error/rcc_error.h"
#include <sys/types.h>

enum 
{
  LLE_OK,
  LLE_ISOPEN = (P_ID_LL_RCC << 8) + 1,
  LLE_ISNOTOPEN,
  LLE_EMPTYLIST,
  LLE_NOTFOUND,
  LLE_NO_CODE,
  LLE_ERROR_FAIL
};

#define LLE_OK_STR         "OK"
#define LLE_ISOPEN_STR     "the library has already been open"
#define LLE_ISNOTOPEN_STR  "the library has not been opened yet"
#define LLE_EMPTYLIST_STR  "the list is empty"
#define LLE_NOTFOUND_STR   "element not found in the list"
#define LLE_NO_CODE_STR    "unknown error code"
#define LLE_ERROR_FAIL_STR "cannot open/close error package"

#define INIT_PAGENUM -1

typedef struct ListElement
{
  int key;                 // key to element
  int page;                // page number
  int next;                // next page (number)
  int loc_data;            // local data
  void *rem_data;          // remote data
  void *ext_data;          // extended remote data
} T_ListElement;

typedef struct ListHeader
{
  T_ListElement *el_base;  // A pointer to the first element in the list
  int el_size;             // The size of a list element in bytes (i.e. sizeof(T_ListElement))
  int fpge;                // The number of the first page in the list
  int lpge;                // The number of the last page in the list
  int nelem;               // The number of elements in the list
} T_ListHeader;

#ifdef __cplusplus
extern "C" {
#endif

// prototypes
err_type LL_Open(void);
err_type LL_Close(void);
err_type LL_AddFirst(T_ListHeader* l_l_head, T_ListElement* el);
err_type LL_AddLast(T_ListHeader* l_l_head, T_ListElement* el);
err_type LL_AddOrdered(T_ListHeader* l_l_head, T_ListElement* el);
err_type LL_RemoveFirst (T_ListHeader* l_l_head, T_ListElement** el_ptr);
err_type LL_RemoveByKey(T_ListHeader* l_l_head, int key, T_ListElement** el_ptr);
err_type LL_RemoveByAdd(T_ListHeader* l_l_head, T_ListElement* el);
err_type LL_Find(T_ListHeader* l_l_head, int key, T_ListElement** el_ptr);
err_type LL_PrintList (T_ListHeader* l_l_head);

#ifdef __cplusplus
}
#endif

#endif

